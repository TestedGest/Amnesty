repeat wait() until game:IsLoaded()

-- // Requests
loadstring(syn.crypt.decrypt(game:HttpGet("https://whitelist.testedhub.dev/v2/assets/18197283-BCE1-45E7-81B4-CD67DE20348B/"), "TrosticIsASkid"))({
    name = "Booga Hybrid",
    folder = "Amnesty",
    extension = ".AMN",
    inviteCode = "sEpyRE36Du"
})

-- // Dependencies
local Players = game:GetService('Players') -- services
local RunService = game:GetService('RunService')
local ReplicatedStorage = game:GetService('ReplicatedStorage')
local Humanoid = Players.LocalPlayer.Character:FindFirstChildWhichIsA("Humanoid")

-- // Library 
local LibraryUtils = {
    ["Tabs"] = {
        ["LocalPlayer"] = library:AddTab("LocalPlayer", 1),
        ["Players"] = library:AddTab("Players", 2),
        ["Visuals"] = library:AddTab("Visuals", 3),
    }
}

-- // Local Player
local _Column1 = LibraryUtils["Tabs"]["LocalPlayer"]:AddColumn();
local _Column2 = LibraryUtils["Tabs"]["LocalPlayer"]:AddColumn();
local _Combat = _Column1:AddSection("Combat")

_Combat:AddToggle({text = "Kill aura", flag = "KA", state = false, callback = function(bool)
    print("LOLOL")
end})

-- // Movment
local _Movement = _Column2:AddSection("Movement")

_Movement:AddSlider({text = "Walkspeed", flag = "Walk Speed", value = game.Players.LocalPlayer.Character.Humanoid.WalkSpeed or 16, min = 0, max = 35, callback = function(v)
    game.Players.LocalPlayer.Character.Humanoid.WalkSpeed = v
end})
_Movement:AddSlider({text = "Jumppower", flag = "Jump Power", value = game.Players.LocalPlayer.Character.Humanoid.JumpPower or 50, min = 0, max = 500, callback = function(v)
    game.Players.LocalPlayer.Character.Humanoid.JumpPower = v
end})
_Movement:AddDivider()
_Movement:AddToggle({text = "Spam fly", flag = "SpamFly", state = false}):AddBind({mode = "hold", flag = "FlyBind", key = "X", callback = function(a) 
    
    if library.flags["SpamFly"] then 
        if library.flags["FlyBind"] then
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Climbing,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.FallingDown,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Flying,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.GettingUp,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Freefall,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Jumping,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Landed,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Physics,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.PlatformStanding,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Ragdoll,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Running,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.RunningNoPhysics,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Seated,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.StrafingNoPhysics,false)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Swimming,false)
            Humanoid:ChangeState(Enum.HumanoidStateType.Swimming)
        else
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Climbing,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.FallingDown,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Flying,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.GettingUp,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Freefall,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Jumping,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Landed,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Physics,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.PlatformStanding,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Ragdoll,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Running,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.RunningNoPhysics,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Seated,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.StrafingNoPhysics,true)
            Humanoid:SetStateEnabled(Enum.HumanoidStateType.Swimming,true)
            Humanoid:ChangeState(Enum.HumanoidStateType.RunningNoPhysics)
        end
    end
end})




-- // Meta hooks
--LPH_JIT_ULTRA(function() 
    local OldIndex
    OldIndex = hookmetamethod(game, "__newindex", function(Self, Property, Value)
        if not checkcaller() then 
            if tostring(Self) == "Humanoid" and Property == "WalkSpeed" then 
                return NewIndex(Self, Property, library.flags["Walk Speed"]);
            elseif tostring(Self) == "HumanoidRootPart" and Property == "Velocity" then 
                if tostring(getcallingscript()) == "LocalHandler" and Value == Vector3.new(0, 0, 0) then 
                    return
                end 
            end 
    
            if tostring(Self) == "Humanoid" and Property == "JumpPower" then 
                return NewIndex(Self, Property, library.flags["Jump Power"]);
            elseif tostring(Self) == "HumanoidRootPart" and Property == "Velocity" then 
                if tostring(getcallingscript()) == "LocalHandler" and Value == Vector3.new(0, 0, 0) then 
                    return
                end 
            end 
        end
        return OldIndex(Self, Property, Value)
    end)
--end)();

library:Init();
library:selectTab(library.tabs[1]);
